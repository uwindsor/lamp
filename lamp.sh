#!/bin/bash
yum -y update
# Need git-core and wget to add additional repos.
yum -y install wget nano man git-core

# Install EPEL Repo.
# wget  http://dl.iuscommunity.org/pub/ius/archive/Redhat/6/x86_64/epel-release-6-5.noarch.rpm
# rpm -ivh epel-release-6-5.noarch.rpm
# Install additional repos from bitbucket.
git clone https://bitbucket.org/uwindsor/yum-repos.git
mv ./yum-repos/*.repo /etc/yum.repos.d
# Clean up our garbage.
rm epel-release-6-5.noarch.rpm
rm -rf ./yum-repos

sudo yum install make gcc httpd php php-pspell php-devel php-pear uploadprogress
sudo yum install mysql mysql-server
sudo yum install php-openssl mod_ssl php-mcrypt
sudo yum install php-xml php-ldap libc-client php-imap php-snmp php-soap php-tidy
sudo yum install phpmyadmin php-eaccelerator pecl_http

sudo pear channel-update pear.php.net
sudo pear install XML_RPC2

pear channel-discover pear.drush.org
pear install drush/drush

sudo chkconfig mysqld on
sudo chkconfig httpd on
