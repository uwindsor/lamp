Installation
=====================================================
In order to retrieve the lamp.sh script, you will likely first have to run:
yum install wget git-core
And then: git clone https://bitbucket.org/uwindsor/lamp.git
If you already have the script locally, you can skip the step above and just
run the script.
Once the lamp.sh script is done:
Edit /etc/php.d/mcrypt.ini
and change
; Enable mcrypt extension module
extension=module.so
to:
; Enable mcrypt extension module
extension=mcrypt.so
Then comment out extension=mcrypt.so from /etc/php, if found.